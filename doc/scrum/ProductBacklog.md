# Product Backlog

Este es un documento de especificación de los requerimientos de este software.

Revisa la [Idea general inicial sobre el aplicativo](../../README.md#Idea-general-inicial-sobre-el-aplicativo)


## Historia: Evaluación de prueba para alumnos

###Descripción
* **Como** profesor **quiero** imprimir mi lista de alumnos **para** poder valorar cúantos alumnos voy a tener que gestionar y evaluar.

* **Como** profesor **quiero** asignar una nota aleatoria a todos mis alumnos distinguiendo de si se trata de una nota de un examen teórico o un examen práctico y verlas en un boletín **para** evaluar cómo se vería estas notas usando este software

###Estimación

*Sin estimar*

###Tests de Aceptación

* **Dado** un menú para el profesor **cuando** este seleccione la opción de asignar notas aleatorias a todos sus alumnos **entonces** debe aparecer un submenú que me de la posibilidad de seleccionar si el examen va a ser Teórico o Práctico. 

* **Dado** en submenú **cuando** selecciona el tipo de examen, **entonces** indicará que las notas ya han sido asignadas a los alumnmos. 

* **Dadas** las notas introducidas **cuando** el profesor selecciones la opción de ver boletín de las notas **entonces** verá la lista de sus alumnos con todas sus notas generadas y la media por cada nota, ordenados por la nota media.


## Historia: Matrícula

###Descripción

**Como** secretario de un centro **quiero** poder matricular a un alumno en una materia **para** que el alumno figure asociado a esa materia, el profesor pueda tenerlo en cuenta.

###Estimación

*Sin estimar*

###Tests de Aceptación

* **Dado** un menú para el secretario **cuando** presiono la opción de Matrícula **entonces** el sistema me indica que introduzca el dni del alumno. La introduzco y presiono Enter

* **Dado** un resultado de la búsqueda con un alumno exitente y con ficha **cuando** presiono Enter para continuar (ESC para cancelar) **entonces** me muestra una lista de materias ofertadas

* **Dado** un alumno NO exixtente y SIN ficha **cuando** presiono la opción de Matrícula **entonces** un mecanismo para dar de alta el alumno con sus datos

* **Dado** la selección de una materia para un alumno **cuando** confirmo que quiero asignar al alumno a la materia **entonces** me muestra una lista de materias en las que ese alumno se ha matriculado


## Historia: Alumno ve sus materias

###Descripción

**Como** alumno **quiero** poder ver las materias en las que estoy matriculado **para** prevenirme de cualquier error en el procesamiento de mi matrícula

###Estimación

*Sin estimar*

###Tests de Aceptación

* **Dado** un menú para el alumno **cuando** este seleccione la opción de listar mis materias **entonces** debe aparecer una lista de materias ordenadas por orden alfabético


##Historia: Secretario asigna materias

###Descripción

**Como** secretario **quiero** poder asignar un profesor a una materia **para** que la imparta y evalue a los alumnos matriculados en dicha materia

###Estimación

*Sin estimar*

###Tests de Aceptación

* **Dado** un menú para el secretario **cuando** presiono la opción de Asignación de profesores **entonces** el sistema me indica que introduzca el código de profesor. Lo introduzco y presiono Enter

* **Dado** un resultado de la búsqueda con un alumno profesor y con ficha **cuando** presiono Enter para continuar (ESC para cancelar) **entonces** me muestra una lista de materias ofertadas

* **Dado** un alumno NO exixtente y SIN ficha **cuando** presiono la opción de Matrícula **entonces** un mecanismo para dar de alta el alumno con sus datos

* **Dado** la lista de materias ofertadas **cuando** selecciono un código de materia **entonces** se asigna esta al profesor y se muestra una lista de las materias ya asignadas y puedo introducir de nuevo un código de materia para hacer una resignación hasta que le indico al sistema que no quiero seguir asignando materias a este profesor.


##Historia: Falta de un alumno

###Descripción

**Como** profesor **quiero** poder introducir una falta de asistencia a un alumno **para** evaluar el seguimiento de las clases y tenerlo en cuenta en la evaluación continuada del alumno

###Estimación

*Sin estimar*

###Tests de Aceptación

* **Dado** un menú para el profesor **cuando** este seleccione la opción de poner falta **entonces** el sistema debe pedir un texto para introducir un criterio de búsqueda por el que se localice al alumno en el sistema.

* **Dado** el texto introducido "agra" **cuando**  presione enter **entonces** debe tener localizado a un alumno que se llama Mateo Agrasar

* **Dado** el texto introducido "agra martin" **cuando** presione enter **entonces** debe tener localizados a unos alumnos que se llaman Martín Fernández y Maria Martinez

* **Dado** el texto introducido "agra martin" **cuando** presione enter **entonces** debe tener localizado a unos alumnos que se llama Martín Fernández, Maria Martinez y Mateo Agrasar

* **Dado** el texto introducido que coincide exactamente con el código de alumno **cuando** presione enter **entonces** el sistema debe introducir directamente la falta del alumno correspondiente a la fecha actual