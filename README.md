# README

## Para qué es este repo?

Este repo plantea un ejercicio sencillo para quien se inicia en la POO, Programación Orienta a Objetos.

Con conocimientos sobre Programación Orienta a Objetos, pretende desde este enfoque de POO, implementar un aplicativo Web que solucione un problema de conocido contexto y que éste no plantee ningún esfuerzo de compresión. El contexto seleccionado es la gestión de realización de exámenes de alumnos y la gestión de sus notas por parte de un profesor. (Ejercicio didáctico).

## Idea general inicial sobre el aplicativo

Vamos a realizar un sistema de gestión de un centro educativo. En este centro habrá alumnos y profesores. Los alumnos están matriculados en diferentes materias que les impartirán uno o varios profesores. En cualquier momento se puede hacer una reasignación de profesores a materias. Las materias comprenden partes evaluables correspondientes a teoría y práctica. Un profesor puede evaluar a través de un examen teórico o práctico al que le asignará una nota numérica. A los exámenes teóricos se les asignará una nota entre 0 y 10 y a los prácticos entre 1 y 5. Finalizada todas las evaluaciones de la materia, la nota final resultante para cada alumno corresponderá a una media ponderada 40% - 60% de teoría y práctica respectivamente. Además de esta nota media, el alumno puede tener la condición de APTO o NO APTO en función de si ha realizado los exámenes suficientes en la materia teórica y práctica y si tiene una nota media igual o superior a 3 en la parte práctica e igual o superior a 5 en la parte teórica. Los exámenes que no realice tendrán una cualificación de 0 en caso de no haberlos realizado. Un alumno puede consultar sus notas en el sistema, por cada examen, por cada tipo de examen, y todas las medias. También puede consultar las materias en las que está matriculado o en las que cursó baja voluntaria o baja de oficio por falta de asistencia. Un profesor puede consultar todos los datos de sus alumnos y también puede consultar los grupos de alumnos por materias. El proceso de matrícula se realiza en secretaría, que cogerá todos los datos del alumnos para crear su ficha e introducirlo en el sistema. Se podrán ver todos los alumnos matriculados en el centro, todos los de un curso anterior y todos los profesores. Tendré una forma de localizar tanto a un alumno como a cualquier persona dentro del sistema y consultar sus datos. A los profesores, a los alumnos y a todos los usuarios del sistema se les proporcionará una cuenta de acceso y una contraseña con acceso las acciones que pueden solicitar cada tipo de usuario. No se requiere una interfaz gráfica de ventanas pero sí una estupenda y atrayente interfaz web. Tiene que ser fácil de manejar y sobretodo rápida, para realizar tareas repetitivas que no impliquen teclear demasiado. El sistema debe guardar los datos para que sean recuperados una vez se re inicie el sistema.

**Nota al pie:** Está deliberadamente redactado en bloque sin espacios o sin estructura para hacer un ejercicio de identificación de clases canditas y comportamientos.

## Concreciones sobre especificaciones


1. Para este caso (el aplicativo web), va a disponer de un sencillo formulario de login (usuario/password). Si las credenciales son correctas se tendrá acceso a la interfaz. Asegúrate de que las credenciales sean estas: usuario: `prof`; usuario: `alu`; usuario: `secr` y en todos los casos el password: `123`.

2. Tu aplicativo debe ser accesible a través del recurso `index.php` existente en la raiz del repo.

3. El formato para concretar las especificaciones se encontrará en forma de [**Product Backlog**](doc/scrum/ProductBacklog.md). Las especificaciones (en este documento) pueden variar o cambiar en el transcurso del desarrollo.

No se dan más indicaciones que las que aquí se expresan. Este documento README puede actualizarse o evolucionar pero si no hay otra especificación tienes libertad para representar la información en la web como más te convenga. En cualquier caso, no contravengas ninguna indicación de estos requisitos. 

Algunas consideraciones: puedes utilizar un único fichero o más de uno. Puedes utilizar mecanismos en el entorno de cliente que estimes (javascript, web dinámica, bootstrap, etc.) pero este ejercicio se centrará en el diseño del backend y no en el entorno del cliente.

## Pre-requisitos

Como requisitos previos se asumen nociones básicas de programación, concretamente este es un buen ejercicio si se parte de un paradigma de programación estructurada, para introducirse en la Programación Orienta a Objetos. El programa tiene una interfaz de shell interactiva.

* Selección de herramientas de programación
* Declaración de variables. Tipo y ámbito.
* Uso de funciones. Principio de Reutilización del Código
* Inserción de código en páginas web
* Estructuras básicas de datos
* Estructuras básicas de control
* Bucles
* Entrada y salida de datos
* Operadores Aritmético-lógicos
* Librerías

## Lo que se pretende aprender de este ejercicio

* Mecanismos de autenticación
* Manejo de Sesiones
* Cookies
* Programación orientada a objetos. Introducción al paradigna en un contexto de programación en PHP
* Clases y Objetos
* Diseño
* Mantenibilidad
* Patrones arquitectónicos
* Patrón MVC (Model-View-Controller)
* Validación y correctitud de datos de entrada
* Librerías

## Antes de ponerte a trabajar...

### Haz un fork del repositorio original

Haz un fork del repositorio original y configúralo de forma privada (la actividad propuesta es individual ;)
Habilita las issues e indica que es un proyecto Php.


### Clona el repositorio

```
git clone <url de tu fork>
```

### Ejecuta el programa

Despliega el directorio en un virtualhost de Apache para que este interprete los scripts.

### Crea tu rama (personal) de trabajo

Crea tu propia rama de trabajo! Crea una nueva rama a partir de master que se llame como el nombre de tu usuario en el curso. Te recuerdo cómo:

```
git checkout -b <usuario>
```

La evolución de tu solución final (si no estás trabajando en equipo) deberá estar apuntada por esta rama. Puedes utilizar todas las ramas que quieras, pero **no trabajes en la master** y asegúrate, si tienes otras ramas que forman parte de tu solución, de combinarlas con tu rama con el nombre de tu usuario.

### Crea tu rama de equipo (solo si se te indica en clase)

Crea una rama para tu equipo! Si se te indica en clase que puedes trabajar en grupo, utiliza esta rama para integrar todo el trabajo tuyo y de tus compañeros. En este caso, la evolución de tu solución final (la tuya y la de tu equipo, será la misma), será la apuntada por esta rama.

## Documenta tu trabajo

El repo debe contener una carpeta nombrada como `doc`. [Sigue las instrucciones](doc/README.md) de cómo documentar.

## Cuándo termines tu trabajo... o eso crees...

### Etiqueta tu versión

Cuando tengas un revisión de tu código que consideres estable, etiquétala de la forma que te indique el [mecanismo de versionado](doc/README.md). Modifica tambien el [changelog](doc/changelog.md) indicando las novedades de la versión.
Puedes hacer etiquetado de tu último commit de la siguiente manera:

```
# Si quieres hacer una etiqueta ligera (solo nombrar un commit
git tag <etiqueta>

# Si quieres hacer una etiqueta que contenga más información
git tag -a <etiqueta> -m 'El mensaje'
```

Si quieres poner una etiqueta a un commit anterior, pon su checksum al final de las instrucciones anteriores.

Recuerda enviar tus tags a tus repos remotos de la siguiente manera:

```
git push <remoto> <tag>
```

Consulta esta [fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado) para más detalles.

## Estrategia de ramificación

Rama					| Uso
------------ 			| -------------
`master`	 			| Evolución del enunciado del ejercicio
`remote\usuario` 	| Evolución de la solución de cada alumno
`remote\teamA..B..C`| Evolución de la solución de cada equipo 
`solucion-A..B..C`	| Rama que representa una solución (puede derivar de master u otra rama)
`examen01`			| Rama donde se plantea un ejercicio de examen sobre este software. Ver [Enunciado Examen](doc/examenes/examen01.pdf)

### Changelog de enunciado:

Se irán etiquetando enunciados consolidados y entregados a alumnos. Se dará una explicación general de cada cambio :

Tag				| Descripción
------------ 	| -------------
`enum-v1`		| Enunciado inicial
`enum-v2`		| Introduje la explicación de crear rama para equipo.
`enum-v3`		| Se introduce una rama donde se plantea un ejercicio de examen. Se actualiza el snapshot del enunciado. Se especifica en los objetivos conocer MVC.


### Snapshot actual del enunciado:

```Shell
.
├── README.md
├── composer.json
├── composer.lock
├── doc
│   ├── Desing.md
│   ├── README.md
│   ├── changelog.md
│   ├── examenes
│   │   └── examen01.pdf
│   └── scrum
│       └── ProductBacklog.md
```

## Contribution guidelines

* Writing tests
* Code review
* Other guidelines

## Who do I talk to?

* Repo owner or admin
* Other community or team contact